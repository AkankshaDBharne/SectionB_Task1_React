**Web Developer Evaluation - 1st Screening
NextGrowthLabs Pvt Ltd**



**Section B: React JS**

Task 1 - Creating the folder structure

**Website Url** : https://folder-structure-task-b1.vercel.app/

**Video Link** : https://drive.google.com/file/d/19d8sSi7MM8XuUmLXuiKFsPCsUtXlnDEF/view?usp=drive_link





**SectionC** -** Bonus Points **- Optional

**1.Write and share a small note about your choice of system to schedule periodic tasks (such as downloading list of ISINs every 24 hours). Why did you choose it? Is it reliable enough; Or will it scale? If not, what are the problems with it? And, what else would you recommend to fix this problem at scale in production?**


**Response** :

Reliability and scalability in a production environment can be ensured by choosing the appropriate system to schedule periodic tasks, like downloading a list of ISINs every 24 hours. 
For which combination of cron jobs and a scripting language like Python is preferred as

•	Cron jobs are widely used and reliable for scheduling recurring tasks.

•	Python is beginner-friendly and well-suited for scripting tasks.

•	Both cron jobs and Python scripts are lightweight which makes them suitable for small to medium-scale periodic tasks.

While cron jobs and Python are reliable and efficient for small to medium-scale tasks, they may have limitations when it comes to scalability and advanced features like

•	scaling issues - they may struggle to handle large-scale tasks efficiently.

•	Lack of Advanced Features

To overcome this ,we can make use of other alternatives that are available like Apache Airflow, Kubernetes CronJobs  that helps to  address scalability and advanced management needs in a production environment.

In conclusion, for achieving reliability and scalability in a production environment, utilizing a combination of cron jobs and Python scripting is a good choice which provide you with various features and comes with certain limitations for large and complex operations .To address this limitations and advanced management needs, various alternatives are available to handle such periodic tasks in production.



**2. Suppose you are building a financial planning tool - which requires us to fetch bank statements from the user. Now, we would like to encrypt the data - so that nobody - not even the developer can view it. What would you do to solve this problem?**

**Response  :**

To securely encrypt and protect user bank statements in a financial planning tool, several steps and best practices can be followed like

•	User Authentication - Implement strong user authentication mechanisms where users should be required to provide secure credentials (e.g., username and password) to securely access their bank statements.

•	Establishing Secure Communication - Encrypt data during transmission using protocols like HTTPS to protect it from interception during transit.

•	Encrypting data when  stored in a database. 

•	Conduct regular security audits and testing to identify vulnerabilities and weaknesses in the system.

•	Implement secure data backup and recovery procedures to prevent data loss.

•	Using Data Encryption algorithm can help in increasing the security

•	Educating users about the importance of protecting their login credentials ,introduction to latest trends to secure data and practicing them.

By following various steps, we can create a robust and secure system for encrypting and protecting user bank statements in your financial planning tool. It's essential to prioritize security at every stage of development to ensure the highest level of data protection.




